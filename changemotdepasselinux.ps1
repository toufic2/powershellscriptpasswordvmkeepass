Import-Module VMware.VimAutomation.Core
#si ancien version de powershell lancer cela a la place
#add-pssnapin VMware.VimAutomation.Core


Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
#Pour vcentersa
#[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls
#Pour matrix
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$cred = Read-Host -Prompt "Entrer le  mot de passe du user read du Vcenter " -AsSecureString
$cred = Convert-FromSecureStringToPlaintext -SecureString $cred

#Mettre le virtualCenter ( xxx ) et le user  ( xxxx) et mot de passe ( xxxxx)
Connect-VIServer -Server xvcenter  -User xuser -Password $cred

#$ErrorActionPreference = 'SilentlyContinue'

#Lister les machine windows allume et de type linux 
$Vmlist = (Get-VM).where{$_.PowerState -eq 'PoweredOn' -and $_.Guest.OSFullName -notmatch 'Windows'}
#Creer un fichier csv avec , comme separateur et une colone vmname
#$Vmlist = import-csv  D:\boulot\fichier\vmlistlinux.csv


$date = Get-Date -format "ddMMyyyy"




function New-RandomPassword {
[CmdletBinding()]
#Switch type of parameter allows us to use just -ParameterName for boolean type of parameters
Param(
    [Parameter()]
    [int]$PasswordLength = 15,
    [Parameter()]
    [switch]$BigLetters,
    [Parameter()]
    [switch]$SmallLetters,
    [Parameter()]
    [switch]$Numbers,
    [Parameter()]
    [switch]$NormalSpecials,
    [Parameter()]
    [switch]$ExtendedSpecials
)
    $asciiTable = $null
    #We are checking if any parameter was provided for function. If not, let's use small leters
    if (!$BigLetters -and !$SmallLetters -and !$Numbers -and !$NormalSpecials -and !$ExtendedSpecials) {
        for ( $i = 97; $i -le 122; $i++ ) {
            $asciiTable += , [char][byte]$i
        }
    }
    if ($Numbers) {
        for ( $i = 48; $i -le 57; $i++ ) {
            $asciiTable += , [char]$i
        }
    }
    #Normal specials have better chance to work in passwords used in some exotic environments
    if ($NormalSpecials) {
        $asciiTable += "*","$","-","+","?","_","&","=","!","%","{","}","/"
    }
    if ($ExtendedSpecials) {
        $asciiTable += "@","#",".",",","^","(",")",":",";","'","`"","~","``","<",">"
    }
    if ($BigLetters) {
        for ( $i = 65; $i -le 90; $i++ ) {
            $asciiTable += , [char]$i
        }
    }
    if ($SmallLetters) {
        for ( $i = 97; $i -le 122; $i++ ) {
            $asciiTable += , [char]$i
        }
    }
    $tempPassword = $null
    for ( $i = 1; $i -le $PasswordLength; $i++) {
        $tempPassword += ( Get-Random -InputObject $asciiTable )
    }
    return $tempPassword
}

#New-RandomPassword -BigLetters -SmallLetters -Numbers -NormalSpecials 15


function results
{
 param([string]$Nom,[string]$Resul)
 $d=New-Object PSObject
 $d | Add-Member -Name NomVM -MemberType NoteProperty -Value $Nom
 $d | Add-Member -Name Resultat -MemberType NoteProperty -Value $Resul 
 
 return $d
}
$Resultats=@()


Function Convert-FromSecureStringToPlaintext ( $SecureString )
{
    [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecureString))
}

###########################################################################
#
# Function to return an entry from a KeePass database.
#
###########################################################################

function Get-KeePassEntryByTitle 
{


    [CmdletBinding()]
    Param 
    ( 
        [Parameter(Mandatory=$true)] [KeePassLib.PwDatabase] $PwDatabase, 
        [Parameter(Mandatory=$true)] [String] $TopLevelGroupName, 
        [Parameter(Mandatory=$true)] [String] $Title, 
        [Switch] $AsSecureStringCredential
    )

    # This only works for a top-level group, not a nested subgroup (lazy).
    $PwGroup = @( $PwDatabase.RootGroup.Groups | where { $_.name -eq $TopLevelGroupName } )
    
    # Confirm that one and only one matching group was found
    if ($PwGroup.Count -eq 0) { throw "ERROR: $TopLevelGroupName group not found" ; return } 
    elseif ($PwGroup.Count -gt 1) { throw "ERROR: Multiple groups named $TopLevelGroupName" ; return } 

    # Confirm that one and only one matching title was found
    $entry = @( $PwGroup[0].GetEntries($True) | Where { $_.Strings.ReadSafe("Title") -eq $Title } )
    if ($entry.Count -eq 0) { throw "ERROR: $Title not found" ; return } 
    elseif ($entry.Count -gt 1) { throw "ERROR: Multiple entries named $Title" ; return } 

    if ($AsSecureStringCredential)
    {
        $secureString = ConvertTo-SecureString -String ($entry[0].Strings.ReadSafe("Password")) -AsPlainText -Force
        [string] $username = $entry[0].Strings.ReadSafe("UserName")
        if ($username.Length -eq 0){ throw "ERROR: Cannot create credential, username is blank" ; return } 
        New-Object System.Management.Automation.PSCredential($username, $secureString) 
    }
    else
    {
        $output = '' | Select Title,UserName,Password,URL,Notes
        $output.Title    = $entry[0].Strings.ReadSafe("Title")
        $output.UserName = $entry[0].Strings.ReadSafe("UserName")
        $output.Password = $entry[0].Strings.ReadSafe("Password")
        $output.URL      = $entry[0].Strings.ReadSafe("URL")
        $output.Notes    = $entry[0].Strings.ReadSafe("Notes")
        $output
    }
}



#Get-KeePassEntryByTitle -PwDatabase $MyKPDatabase -TopLevelGroupName Windows -Title ABACA
#Get-KeePassEntryByTitle -PwDatabase $MyKPDatabase -TopLevelGroupName linux -Title AIL -AsSecureStringCredential 


###########################################################################
#
# Function to add an entry to a KeePass database.
#
###########################################################################

function New-KeePassEntry
{


    [CmdletBinding(DefaultParametersetName="Plain")]
    Param 
    ( 
        [Parameter(Mandatory=$true)] [KeePassLib.PwDatabase] $PwDatabase, 
        [Parameter(Mandatory=$true)] [String] $TopLevelGroupName, 
        [Parameter(Mandatory=$true)] [String] $Title, 
        [Parameter(ParameterSetName="Plain")] [String] $UserName,
        [Parameter(ParameterSetName="Plain")] [String] $Password,
        [Parameter(ParameterSetName="Cred")]  [System.Management.Automation.PSCredential] $PSCredential,
        [String] $URL,
        [String] $Notes,
	#	[String] $Attachments
    )


    # This only works for a top-level group, not a nested subgroup:
    $PwGroup = @( $PwDatabase.RootGroup.Groups | where { $_.name -eq $TopLevelGroupName } )

    # Confirm that one and only one matching group was found
    if ($PwGroup.Count -eq 0) { throw "ERROR: $TopLevelGroupName group not found" ; return } 
    elseif ($PwGroup.Count -gt 1) { throw "ERROR: Multiple groups named $TopLevelGroupName" ; return } 
    
    # Use PSCredential, if provided, for username and password:
    if ($PSCredential)
    {
        $UserName = $PSCredential.UserName
        $Password = [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($PSCredential.Password))
    }

    # The $True arguments allow new UUID and timestamps to be created automatically:
    $PwEntry = New-Object -TypeName KeePassLib.PwEntry( $PwGroup[0], $True, $True ) 

    # Protected strings are encrypted in memory:
    $pTitle = New-Object KeePassLib.Security.ProtectedString($True, $Title)
    $pUser = New-Object KeePassLib.Security.ProtectedString($True, $UserName)
    $pPW = New-Object KeePassLib.Security.ProtectedString($True, $Password)
    $pURL = New-Object KeePassLib.Security.ProtectedString($True, $URL)
    $pNotes = New-Object KeePassLib.Security.ProtectedString($True, $Notes)
 #   $pAttachments = New-Object KeePassLib.Security.ProtectedString($True, $Attachments)


    $PwEntry.Strings.Set("Title", $pTitle)
    $PwEntry.Strings.Set("UserName", $pUser)
    $PwEntry.Strings.Set("Password", $pPW)
    $PwEntry.Strings.Set("URL", $pURL)
    $PwEntry.Strings.Set("Notes", $pNotes)
#	$PwEntry.Strings.Set("Attachments", $pAttachments)

    $PwGroup[0].AddEntry($PwEntry, $True)

    # Notice that the database is automatically saved here!
    $StatusLogger = New-Object KeePassLib.Interfaces.NullStatusLogger
    $PwDatabase.Save($StatusLogger) 

}


#New-KeePassEntry -PwDatabase $MyKPDatabase -TopLevelGroupName 'Linux' -Title 'AIL' -UserName "root" -Password "Pazzwurd" # -URL "http://www.sans.org/sec505" -Notes "Some notes here."   

#$cred = Get-Credential
#New-KeePassEntry -PwDatabase $MyKPDatabase -TopLevelGroupName 'Windows' -Title 'ALETRIS' -PSCredential $cred 




cd "C:\Program Files (x86)\KeePass Password Safe 2"
(Get-ChildItem -recurse $script:myinvocation.mycommand.path | Where-Object {($_.Extension -EQ ".dll") -or ($_.Extension -eq ".exe")} | ForEach-Object { $AssemblyName=$_.FullName; Try {[Reflection.Assembly]::LoadFile($AssemblyName) } Catch{ }} ) | out-null



$MyKPDatabase = new-object KeePassLib.PwDatabase
#$MyMasterPassword = "*******"
$MyMasterPassword = Read-Host -Prompt "Entrer le mot de passe keepass " -AsSecureString 
$MyMasterPassword = Convert-FromSecureStringToPlaintext -SecureString $MyMasterPassword
$MyKPKey = new-object KeePassLib.Keys.CompositeKey
$MyKPKey.AddUserKey((New-Object KeePassLib.Keys.KcpPassword($MyMasterPassword)));
$IOConnectionInfo = New-Object KeePassLib.Serialization.IOConnectionInfo
$IOCOnnectionInfo.Path = "D:\DB\BDD.kdbx"
$KPNStatusLogger = New-Object KeePassLib.Interfaces.NullStatusLogger

$MyKPDatabase.Open($IOCOnnectionInfo,$MyKPKey,$KPNStatusLogger)

#Fermer la base $MyKPDatabase.Close()

#Si le mot de passe est tjrs le meme
#$pass = Read-Host -Prompt "Entrer le  mot de passe linux" -AsSecureString
#$pass = Convert-FromSecureStringToPlaintext -SecureString $pass

foreach ($vm in $Vmlist) {    


#$vvm = $vm.vmname #si on utilise le fichier csv 
$vvm = $vm
$vpassword = New-RandomPassword -BigLetters -SmallLetters -Numbers -NormalSpecials 15

$pass = (Get-KeePassEntryByTitle -PwDatabase $MyKPDatabase -TopLevelGroupName Linux -Title $vvm).Password


$update = "echo '$vpassword' | passwd root --stdin"


Invoke-VMScript -VM $vvm -ScriptText $update -GuestUser root -GuestPassword $pass -scripttype bash
Try{
        $out = Invoke-VMScript -ErrorAction Stop -VM $vvm -ScriptText 'ls' -ScriptType Bash -GuestUser root -GuestPassword $vpassword
        $result = "changement mot de passe reussi"
		Write-host "$vvm $result"
    }
    Catch{
        $out = ''
        $result = "Erreur changement mot de passe"
		Write-host "$vvm $result"
    }
}



#Fermer la base 

$MyKPDatabase.Close()

